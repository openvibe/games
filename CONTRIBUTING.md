# Authors

## Author list

- [Thibaut Monseigne](https://tmonseigne.github.io/) ([Inria](https://www.inria.fr/fr))

## Games

- [Hello World](https://gitlab.inria.fr/openvibe/unity-games/hello-world) - [Thibaut Monseigne](https://tmonseigne.github.io/) ([Inria](https://www.inria.fr/fr)) [![pipeline status](https://gitlab.inria.fr/openvibe/unity-games/hello-world/badges/master/pipeline.svg)](https://gitlab.inria.fr/openvibe/unity-games/hello-world/pipelines)
[![Documentation](https://img.shields.io/badge/Documentation-deploy-success)](https://openvibe.gitlabpages.inria.fr/unity-games/hello-world/)
- [Hello Sender](https://gitlab.inria.fr/openvibe/unity-games/hello-sender) - [Thibaut Monseigne](https://tmonseigne.github.io/) ([Inria](https://www.inria.fr/fr)) [![pipeline status](https://gitlab.inria.fr/openvibe/unity-games/hello-sender/badges/master/pipeline.svg)](https://gitlab.inria.fr/openvibe/unity-games/hello-sender/pipelines)
[![Documentation](https://img.shields.io/badge/Documentation-deploy-success)](https://openvibe.gitlabpages.inria.fr/unity-games/hello-sender/)
- [Hello Bidirectionnal](https://gitlab.inria.fr/openvibe/unity-games/hello-bidirectionnal) - [Thibaut Monseigne](https://tmonseigne.github.io/) ([Inria](https://www.inria.fr/fr)) [![pipeline status](https://gitlab.inria.fr/openvibe/unity-games/hello-bidirectionnal/badges/master/pipeline.svg)](https://gitlab.inria.fr/openvibe/unity-games/hello-bidirectionnal/pipelines)
[![Documentation](https://img.shields.io/badge/Documentation-deploy-success)](https://openvibe.gitlabpages.inria.fr/unity-games/hello-bidirectionnal/)

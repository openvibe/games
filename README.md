# OpenViBE Games

Set of external games usable with OpenViBE.

## Requirements

- Unity 2019 and Higher (Pipeline for continuous integration use Unity 2019.4.18f1).

## Creating a new game

### On Gitlab Inria Website

- Create a new public[^1] empty project in [Unity Apps](https://gitlab.inria.fr/openvibe/unity-games) group.
- Add License with the template `GNU Affero General Public License v3.0`  
Commit with the message `:page_facing_up: Add LICENSE` (see [gitmoji](https://gitmoji.carloscuesta.me/))
- Go to `Settings->General->Visibility, project features, permissions` activate Pipelines
- Go to `Settings->Repository->Protected Branches` suppress the protection of the master branch if you want.
- Go to `Settings->CI/CD->Variables` and add three variables.
  - Key `SonarHostUrl` : Value `https://sonarqube.inria.fr/sonarqube`
  - Key `SonarProjectKey` : Value `inria:openvibe:games:yourgame`
  - Key `SonarLogin` : Value `Your Token`. token created on [sonarqube website](https://sonarqube.inria.fr/sonarqube/account/security/) (Check the Masked switch).

### On your repository

- Copy The Hello World Game Repository to your repository with the commit message `:tada: First Commit with Hello World Repo` (your pipeline fail normally, you can verify if you receive a mail or not in this case.)
- Replace the Hello World Occurence by your Game : (find `hello-world`, `hello world`, `helloworld`) for namespace, project name, link to repository... *don't forget `yml` and `json` file*. Now the pipeline works.

### On SonarQube Inria Website

- Go to your Project `inria:openvibe:games:yourgame`
- Got to `Administration->Quality Profile->C#` and select inria:openvibe
- Got to `Administration->Permissions` and select Public[^1]


### On the Games Repository

- Add a submodule with the https adress of your game repository ([https://gitlab.inria.fr/openvibe/unity-games/your-game.git](https://gitlab.inria.fr/openvibe/unity-games/your-game.git))
- Add the game in contributing markdown file (`CONTRIBUTING.md`).
- Commit with the message `:sparkles: Your Game` when your game is finished (or stable for this version to avoid a great number of commit on this repository)

[^1]: We are Open Source Project.
